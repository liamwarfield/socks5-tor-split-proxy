# Socks 5 split proxy
 
This is a simple proxy that examines if traffic is being
proxied to a tor hidden service that's hosted locally.
This server will asyncronusly spin off proxys, so it supports
multiple simultanios connections. This split proxy does not
handle ANY type of auth for the SOCKS5 proxy.
 
Run with RUST_LOG=info to get the broad strokes of
whats happening.
 
 
## How is it implemented
This project uses a modified version of (fast socks5)[https://github.com/dizda/fast-socks5].
I chose this library because it was fairly simple to understand,
and because it was MIT licensed. (using https://en.wikipedia.org/wiki/SOCKS) My approach was
let the SOCKS5 handshake proceed normally up to the server receiving `Client connection request`
packet. At this point my code will either:
 
1. See that the destination is for a hidden service hosted on the same computer and make a TCP
connection to `localhost:<hidden service port>`.
2. (or) Set up a SOCKS5 client with the Tor daemon. The client provide by `fast-socks5` implements
`AsyncRead + AsyncWrite` so I just treat it the same as the TCP stream in option 1.
 
After setting up the appropriate stream, the rest of the hand shake is completed as normal.
The client is not aware that the destination of the traffic has changed.

Time taken to complete: 
6hrs of coding
2hrs of research on protocals/setting up a hidden service to test with
 
My modifications to `fast-socks5` start at
`fast-socks5/src/server.rs:220`
