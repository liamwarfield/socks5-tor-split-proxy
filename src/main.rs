extern crate pretty_env_logger;
#[macro_use]
extern crate log;

use async_std::{future, net::TcpStream};
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::str::FromStr;

use async_std::{stream::StreamExt, task};
use fast_socks5::client::Socks5Stream;
use fast_socks5::util::target_addr::TargetAddr;
use fast_socks5::{
    server::{Config, Socks5Server, Socks5Socket},
    Result,
};

use lazy_static::lazy_static;

use regex::Regex;

fn main() -> Result<()> {
    pretty_env_logger::init();
    task::block_on(spawn_socks_server())
}

async fn spawn_socks_server() -> Result<()> {
    let mut config = Config::default();
    config.set_request_timeout(10);
    config.set_skip_auth(false);

    let mut listener = Socks5Server::bind("127.0.0.1:1080").await?;
    listener.set_config(config);

    let mut incoming = listener.incoming();

    info!("Listening for socks connections 127.0.0.1:1080");

    // Standard TCP loop
    while let Some(socket_res) = incoming.next().await {
        match socket_res {
            Ok(socket) => {
                spawn_intercept_and_log_error(socket);
            }
            Err(err) => {
                println!("accept error = {:?}", err);
            }
        }
    }

    Ok(())
}

fn spawn_intercept_and_log_error(socket: Socks5Socket<TcpStream>) -> task::JoinHandle<()> {
    task::spawn(async move {
        let hostnames: HashMap<String, usize>;
        match socket.intercept_upgrade().await {
            Ok(socket) => {
                match get_hidden_hostnames() {
                    Ok(h) => hostnames = h,
                    Err(e) => {
                        error!("{:#}", e);
                        hostnames = HashMap::new();
                    }
                }
                if let Some(destination) = socket.target_addr() {
                    debug!("There was a destination: {:?}", destination);
                    match destination {
                        TargetAddr::Domain(s, _u) => {
                            if let Some(port) = hostnames.get(s) {
                                // This is so that you can see that it found the service.
                                // In real life I would not long the name of the hidden
                                // service
                                info!("Found matching hidden service:{}", s);
                                info!("Forwarding trafic directly to service");
                                // Open up at direct TCP stream 127.0.0.1:port
                                let outbound = match future::timeout(
                                    std::time::Duration::from_secs(10),
                                    TcpStream::connect(format!("127.0.0.1:{}", port)),
                                )
                                .await
                                {
                                    Ok(e) => match e {
                                        Ok(o) => o,
                                        Err(e) => {
                                            error!("TCP Error: {:#}", e);
                                            return;
                                        }
                                    },
                                    Err(_e) => {
                                        error!("TCP timeout with hidden service");
                                        return;
                                    }
                                };
                                //upgrade the connection with direct connection to the hidden service
                                match socket.continue_upgrade(outbound).await {
                                    Ok(_) => {}
                                    Err(e) => error!("{:#}", &e),
                                };
                            } else {
                                connect_to_tor_daemon(destination.clone(), socket).await;
                            }
                        }
                        _ => {
                            connect_to_tor_daemon(destination.clone(), socket).await;
                        }
                    }
                }
            }
            Err(err) => {
                error!(
                    "Error occured before SOCKS destination could be intercepted {:#}",
                    err
                );
            }
        }
    })
}

// Sets up a socks client with the tor daemon on port 9050
async fn connect_to_tor_daemon(target_address: TargetAddr, socket: Socks5Socket<TcpStream>) {
    let addr;
    let port;
    match target_address {
        TargetAddr::Ip(ip_addr) => {
            addr = ip_addr.ip().to_string();
            port = ip_addr.port();
        }
        TargetAddr::Domain(s, u) => {
            addr = s.to_string();
            port = u.to_owned();
        }
    }
    info!(
        "Sending to the traffic to the tor deamon: {}:{}",
        addr, port
    );
    let config = fast_socks5::client::Config::default();
    let socks = match Socks5Stream::connect("127.0.0.1:9050", addr, port, config).await {
        Ok(s) => s,
        Err(e) => {
            println!("{:?}", e);
            return;
        }
    };
    info!("tor SOCKS5 successfully client created");

    if let Err(e) = socket.continue_upgrade(socks).await {
        error!("{:#}", &e);
    }
    return;
}

// Returns a list of hostnames of the hidden services running
fn get_hidden_hostnames() -> Result<HashMap<String, usize>, String> {
    let mut hs_dirs: Vec<(String, usize)> = Vec::new();
    let mut hidden_hostnames: HashMap<String, usize> = HashMap::new();
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^HiddenServiceDir (\S*)").unwrap();
        static ref PORT_RE: Regex = Regex::new(r"^HiddenServicePort (\d*)").unwrap();
    }

    // This is really rough, if this were production code I would
    // take more time to adhere to https://helpmanual.io/man5/torrc/
    // But for now I'm going to get away with the quick and dirty assumption
    // that there are no + in the config, and that port always comes after
    // service.
    if let Ok(file) = File::open("/etc/tor/torrc") {
        let reader = BufReader::new(file).lines();
        let mut last_dir = String::new();
        for line in reader {
            if let Ok(cf_line) = line {
                for cap in RE.captures_iter(&cf_line) {
                    last_dir = cap[1].to_string();
                }
                for cap in PORT_RE.captures_iter(&cf_line) {
                    cap[1].to_string();
                    // naked unwrap, bad. But again making a config parser was
                    // not the chalange
                    hs_dirs.push((last_dir.clone(), usize::from_str(&cap[1]).unwrap()));
                }
            }
        }
    } else {
        error!("Could not open torrc");
        return Err("Could not open torrc".to_string());
    }

    for (hs, port) in hs_dirs {
        let hs_path = Path::new(&hs).join("hostname");
        if let Ok(mut file) = File::open(hs_path) {
            let mut contents = String::new();
            match file.read_to_string(&mut contents) {
                Err(_) => println!("Failed to read hostname for {:?}", hs),
                _ => {
                    hidden_hostnames.insert(contents.trim().to_string(), port);
                }
            };
        } else {
            warn!("Could not get hostname tor hidden service: {:?}", hs);
        }
    }

    Ok(hidden_hostnames)
}
